# NextGEMS Meeting Times

Scheduling repeated meetings across timezones can be quite puzzling because of different switching dates for daylight saving time (if any).
This repository computes meeting times in various time zones automatically, such that ambiguities are hopefully resolved.

## background

Different countries have different rules for daylight saving times. If a repeated date should be scheduled in a way which tries to keep the local time constant over the course of a year, the meeting has to be scheduled in some local time zone. Still, meeting times can change for some areas on the globe if (a) counties don't use daylight saving time or (b) the date on which daylight saving time is switched on or off is different as compared to the "scheduling time zone".

The [Internet Assigned Numbers Authority](https://www.iana.org/time-zones) maintains a continuously updated database of time zones used across the globe. Time zones in this database are defined as areas in which local time has been consistent since 1970 (and sometimes beyond). The database contains all rules for changes in local time zones (including daylight saving time) as well as all leap second insertions. Howard Hinnant's [date](https://github.com/HowardHinnant/date) library parses this entire database and thus greatly facilitates the task of computing times in various areas correctly. This library has also been merged into standard C++ since C++20.

Please have a look into `meeting_time.cpp` for the details or just loot at the [generated table](https://tobi_public.pages.gwdg.de/nextgems_meeting_time/meeting_time.txt).
