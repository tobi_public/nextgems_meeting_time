#include <chrono>
#include <date/tz.h>
#include <iostream>
#include <iomanip>
#include <sstream>

// using date = std::chrono

using namespace date;
using namespace std::chrono_literals;

int main() {
    auto reference_zone = locate_zone("Europe/Berlin");
    auto now_local = make_zoned(reference_zone, std::chrono::system_clock::now());
    auto ymwd = year_month_weekday{floor<days>(now_local.get_local_time())};

    auto this_weeks_meeting_day = Wednesday[ymwd.index()]/ymwd.month()/ymwd.year();

    auto output_zones = std::vector{
        locate_zone("Europe/Berlin"),
        locate_zone("Europe/London"),
        locate_zone("Europe/Madrid"),
        locate_zone("Europe/Stockholm"),
        locate_zone("Europe/Helsinki"),
        locate_zone("Europe/Warsaw"),
        locate_zone("Europe/Lisbon"),
        locate_zone("Africa/Dakar"),
        locate_zone("Europe/Zurich"),
        locate_zone("Europe/Oslo"),
        locate_zone("Europe/Copenhagen"),
        locate_zone("Europe/Paris"),
        locate_zone("Europe/Amsterdam")
    };

    std::cout << "upcoming NextGEMS office hours:\n";
    std::cout << "===============================\n\n";
    for(int i = 0; i < 3; ++i) {
        auto meeting_local = make_zoned(reference_zone, local_days{this_weeks_meeting_day} + 15h + std::chrono::weeks{i});
        for (const auto& zone: output_zones) {
            std::stringstream s;
            s << make_zoned(zone, meeting_local);
            std::cout << std::left << std::setw(30) << zone->name() << ": ";
            std::cout << std::left << std::setw(30) << s.str() << "\n";
        }
        std::cout << "\n";
    }
}
